#!/usr/bin/env ruby
require 'logger'
require 'psych'
require 'json'
require 'redis'
require 'typhoeus'



def readAndWrite(pathToFile, redis, logger)
  filecontent = IO.read(pathToFile)
  logger.debug("replaying trace #{pathToFile}")
  puts "starting file #{pathToFile}"
  Psych.load_stream(filecontent) do |object|
    logger.debug(object)
    if object.key?('log') then
      logger.debug('log')
      traceuuid||=object.dig('log','trace','cpee:uuid')
      traceuuid||=object.dig('log','trace','cpee:instance')
      traceuuid||=-1
      logger.debug(traceuuid)
      p traceuuid
      #in redis list pushen
      redis.rpush(traceuuid, object.to_json)
    elsif object.key?('event') then
      traceuuid||=object.dig('event','cpee:uuid')
      traceuuid||=object.dig('event','cpee:instance')
      traceuuid||=-1
      #in redis list pushen
      redis.rpush(traceuuid, object.to_json)
      if object.dig('event','cpee:lifecycle:transition')=='activity/receiving' && object.dig('event', 'list', 'data_receiver', 0, 'data').is_a?(Hash) && !object.dig('event', 'list', 'data_receiver', 0, 'data', 'CPEE-INSTANCE-UUID').nil? && object.dig('event', 'list', 'data_receiver', 0, 'data', 'CPEE-STATE')=='running' then
        p object.dig('event', 'list', 'data_receiver', 0, 'data', 'CPEE-INSTANCE-UUID')
        logger.debug("subprocess - going down one level to #{object.dig('event', 'list', 'data_receiver', 0, 'data', 'CPEE-INSTANCE-UUID')}")
        p "going to #{object.dig('event', 'list', 'data_receiver', 0, 'data', 'CPEE-INSTANCE-UUID')}\n"
        readAndWrite("#{File.dirname(pathToFile)}/#{object.dig('event', 'list', 'data_receiver', 0, 'data', 'CPEE-INSTANCE-UUID')}.xes.yaml", redis, logger)
      end
      if object.dig('event','cpee:lifecycle:transition')=='task/instantiation' && object.dig('event', 'data', 'data_receiver').is_a?(Hash) && !object.dig('event', 'data', 'data_receiver', 'CPEE-INSTANCE-UUID').nil? then
        puts object.dig('event', 'data', 'data_receiver', 'CPEE-INSTANCE-UUID')
        logger.debug("subprocess - going down one level to #{object.dig('event', 'data', 'data_receiver', 'CPEE-INSTANCE-UUID')}")
        puts "going to #{object.dig('event', 'data', 'data_receiver', 'CPEE-INSTANCE-UUID')}\n"
        readAndWrite("#{File.dirname(pathToFile)}/#{object.dig('event', 'data', 'data_receiver', 'CPEE-INSTANCE-UUID')}.xes.yaml", redis, logger)
      end
    else
      logger.debug('unknown type')
      puts 'unknown type'
    end
  end
end


logger = Logger.new File.new('replayLogLocal.log', 'w')
redis=Redis.new(
  :path => "/tmp/redis.sock",
  :encoding => 'utf-8'
)
#redis.select 1
#redis.select 2
#redis.select 3
#redis.select 4
redis.select 15

#readAndWrite('gv12/logs/batch15/cb3c805e-cb98-4df1-adf7-cf6ab0e8f5af.xes.yaml', redis, logger)
readAndWrite('gv12/logs/batch15/f47bb3a9-b606-475e-8b43-ee981065dcb5.xes.yaml', redis, logger)
readAndWrite('gv12/logs/batch15/679414e0-ab25-430c-8016-11ed04f28bb2.xes.yaml', redis, logger)
#readAndWrite('gv12/logs/batch14/dfe9fb77-532b-48f2-a41c-c9b47b206d80.xes.yaml', redis, logger)
#readAndWrite('gv12/logs/batch14/9fcfeaff-ecd7-4c53-a766-72d0fcbdf8b5.xes.yaml', redis, logger)
#readAndWrite('gv12/logs/batch12/d4381ad5-1ecc-41b1-810e-e380b346f9d7.xes.yaml', redis, logger)
#readAndWrite('gv12/logs/batch12/ab1fda08-387d-49fa-aa8d-e640c6f602aa.xes.yaml', redis, logger)
#readAndWrite('gv12/logs/batch12/253daced-3223-454d-897c-ef50afc71755.xes.yaml', redis, logger)
#readAndWrite('gv12/logs/batch11/1e85deb8-2484-4f48-9263-eac3685002fc.xes.yaml', redis, logger)
#readAndWrite('gv12/logs/batch11/7e0cf9d9-ff9b-4751-b395-e478d45bc0af.xes.yaml', redis, logger)
=begin
Plain Instance (f47bb3a9-b606-475e-8b43-ee981065dcb5) - 1791

Plain Instance (679414e0-ab25-430c-8016-11ed04f28bb2) - 2518

  Spawn GV12 Production (cb3c805e-cb98-4df1-adf7-cf6ab0e8f5af) - 1919
    GV12 Turn Production (d97d437d-4a59-4eff-b224-9e8afbf363b9) - 1920
      GV12 Turn Machining (d5728109-6e9a-45b5-9461-2fb16a90b4b1) - 1928
    GV12 Keyence Measurement (958383f2-d122-4406-b412-a32f80643794) - 1933
      GV12 Keyence Measuring (6005cdae-2019-4eb9-a188-b558960caf40) - 1953

  Spawn GV12 Production (9edab423-683f-42a8-868b-f825c829b781) - 1946
    GV12 Turn Production (8492780c-0300-4184-9f6d-ecadfc96d7fb) - 1947
      GV12 Turn Machining (68f6bf47-2899-4c40-ad44-66e3e1684b86) - 1955
    GV12 Keyence Measurement (640e1615-4c17-4d39-bfe3-bf78d6e0484d) - 1960
      GV12 Keyence Measuring (890bc258-840b-4c70-abd2-b7b8573e6cfb) - 1982
=end
