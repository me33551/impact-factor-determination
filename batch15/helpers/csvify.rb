#!/usr/bin/env ruby
require 'logger'
require 'json'
require 'csv'

logger = Logger.new File.new('csvify.log', 'w')

if(__FILE__ == $0) then
  if(ARGV.length == 2) then
    traces = Dir.new(File.join(__dir__,"#{ARGV[1]}"))
    counter = 0
    keys = nil
    csv_string = CSV.generate do |csv|
      traces.each_child do |trace|
        matchObject = trace.match(/(.+)_result.json$/)
        uuid = matchObject.nil? ? nil : matchObject[1]
        result = JSON.load(File.open(File.join(__dir__,"#{ARGV[1]}", "#{uuid}_result.json"),'r+'))
        if(counter == 0) then
          keys = result.keys
          keys.delete_if {|key| key.include?("dtw")}
          p keys
          csv << ['uuid'] + keys
          p ['uuid'] + keys
        end
        values = []
        values.push(uuid)
        keys.each do |key|
          values.push(!result[key].is_a?(Hash) ? result[key] : result[key]['value'])
        end
        p values
        csv << values


        counter+=1
      end
    end
    p csv_string
    file = File.open(File.join(__dir__,"csvs",ARGV[0]),"w+")
    file.write(csv_string)
  else
    p "wrong number of arguments"
    p ARGV
  end
end
