#!/usr/bin/env ruby
require 'riddl/server'
require 'json'
require 'dtw'

def compactData(orderedData, precision=1) #{{{
  shorterDataObject = {'data' => []}
  dataObject = orderedData
  while(!dataObject['data'].empty?) do
    temp = dataObject['data'].shift(precision)
    avgValue = temp.inject(0.0) {|sum,element| (sum+element['value'])}/temp.length()
    shorterDataObject['data'].push({'value' => avgValue, 'timestamp' => temp[(temp.length-1)/2]['timestamp']})
  end
  return shorterDataObject
end #}}}

def getFullData(orderedData, precision=1) #{{{
  # orderedData should be ordered array in the format of -> [{'value','timestamp'}, ...]
  span = ((DateTime.strptime(orderedData.last['timestamp'], '%Y-%m-%dT%H:%M:%S.%L').to_time() - DateTime.strptime(orderedData.first['timestamp'], '%Y-%m-%dT%H:%M:%S.%L').to_time())*1000).to_i

  lastValue = orderedData.first['value']
  dataObject = {'data' => []}
  (span+1).times do |index|
    ms = 1.0/24/60/60/1000
    currentTimestamp = (DateTime.strptime(orderedData.first['timestamp'], '%Y-%m-%dT%H:%M:%S.%L') + index*ms).strftime('%Y-%m-%dT%H:%M:%S.%L')
    elementIndex = orderedData.index do |el|
      el['timestamp'] == currentTimestamp
      #(DateTime.strptime(el['timestamp'], '%Y-%m-%dT%H:%M:%S.%L') < DateTime.strptime(currentTimestamp, '%Y-%m-%dT%H:%M:%S.%L'))
      #(DateTime.strptime(el['timestamp'], '%Y-%m-%dT%H:%M:%S.%L') >= DateTime.strptime(currentTimestamp, '%Y-%m-%dT%H:%M:%S.%L'))
    end
    if(!elementIndex.nil?) then
      lastValue = orderedData[elementIndex]['value']
    end
    usedValue = lastValue
    dataObject['data'].push({'value' => usedValue, 'timestamp' => currentTimestamp})
  end

  shorterDataObject = {'data' => []}
  while(!dataObject['data'].empty?) do
    temp = dataObject['data'].shift(precision)
    avgValue = temp.inject(0.0) {|sum,element| (sum+element['value'])}/temp.length()
    shorterDataObject['data'].push({'value' => avgValue, 'timestamp' => temp[(temp.length-1)/2]['timestamp']})
  end
  return shorterDataObject
  #return dataObject
end #}}}

def getTimeSlotBasedOnBoundaries(orderedData, startPoint, stopPoint) #{{{
  # startPoint and endPoint in ms from first data point
  timeSlotDataObject = {'data' => []}
  first = DateTime.strptime(orderedData['data'].first['timestamp'], '%Y-%m-%dT%H:%M:%S.%L').to_time().to_f*1000
  last = DateTime.strptime(orderedData['data'].last['timestamp'], '%Y-%m-%dT%H:%M:%S.%L').to_time().to_f*1000
  changed = false
  lastObject = orderedData['data'].first
  # first element has to be one that acutally exists (i.e., first element is the first 'real' element being in the range between start and stop point) - this is detected by taking the first element in the range that changes
  orderedData['data'].each do |obj|
    actual = DateTime.strptime(obj['timestamp'], '%Y-%m-%dT%H:%M:%S.%L').to_time().to_f*1000
    if(actual>=(first+startPoint-1) && actual<=(first+stopPoint)) then
      if(!changed && lastObject['value'] != obj['value']) then
        changed = true
      end
      #p obj['timestamp']
      timeSlotDataObject['data'].push(obj) unless !changed
    end
    lastObject = obj
  end
  return timeSlotDataObject
end #}}}

def getTimeSlotBasedOnGaps(orderedData, valueSpanAfterGap) #{{{
  timeSlotDataObject = {'data' => []}
  counter = 0
  active = false
  maybeActiveChange = false
  wasActive = false
  lastObject = orderedData['data'].first
  orderedData['data'].each do |obj|
    if(true) then
      if(obj['value'] == 999.99) then
        maybeActiveChange = true
        if active then active = false end
      end
      if(maybeActiveChange && obj['value'] != 999.99) then
        counter += 1
        if(obj['value'] > valueSpanAfterGap.first && obj['value'] < valueSpanAfterGap.last && counter >=3) then
          #p obj['value']
          active = true unless wasActive
          wasActive = true
        else
          #p obj['value']
          active = false
        end
        maybeActiveChange = false
        #p active
      end
      #p obj['timestamp']
      timeSlotDataObject['data'].push(obj) unless !active
    end
    lastObject = obj
  end
  return timeSlotDataObject
end #}}}

def getDpNumber(dataJson, exceptions=[]) #{{{
  dataObject=JSON.parse(dataJson)
  if(!dataObject['data'].nil?) then
    exceptions.each do |exception|
      dataObject['data'].delete_if {|element| element['value'] == exception}
    end
    result = dataObject['data'].length()
  else
    result = nil
  end
  return result
end #}}}

def getMin(dataJson, exceptions=[]) #{{{
  dataObject=JSON.parse(dataJson)
  if(!dataObject['data'].nil?) then
    exceptions.each do |exception|
      dataObject['data'].delete_if {|element| element['value'] == exception}
    end
    result = (dataObject['data'].min {|a, b| a['value'] <=> b['value']})
  else
    result = nil
  end
  return result
end #}}}

def getMax(dataJson, exceptions=[]) # {{{
  dataObject=JSON.parse(dataJson)
  if(!dataObject['data'].nil?) then
    exceptions.each do |exception|
      dataObject['data'].delete_if {|element| element['value'] == exception}
    end
    result = (dataObject['data'].max {|a, b| a['value'] <=> b['value']})
  else
    result = nil
  end
  return result
end #}}}

def getAvg(dataJson, exceptions=[]) #{{{
  dataObject=JSON.parse(dataJson)
  if(dataObject['data'].nil? || dataObject['data'].length == 0) then
    return nil
  else
    exceptions.each do |exception|
      dataObject['data'].delete_if {|element| element['value'] == exception}
    end
    result = (dataObject['data'].inject(0.0) {|sum,element| (sum+element['value'])}/dataObject['data'].length()).round(2)
    return result
  end
end #}}}

def getWgtdAvg(dataJson, exceptions=[]) #{{{
  preDataObject=JSON.parse(dataJson)
  if(preDataObject['data'].nil? || preDataObject['data'].length == 0) then
    return nil
  end
  orderedData = preDataObject['data'].sort do |a,b|
    DateTime.strptime(a['timestamp'], '%Y-%m-%dT%H:%M:%S.%L') <=> DateTime.strptime(b['timestamp'], '%Y-%m-%dT%H:%M:%S.%L')
  end

  dataObject = getFullData(orderedData)

  #p dataObject['data'].length
  #p dataObject['data'].first
  #p dataObject['data'].last
  #p dataObject['data'].index{|el| el['value']==999.99}

  if(dataObject['data'].nil? || dataObject['data'].length == 0) then
    return nil
  else
    exceptions.each do |exception|
      #dataObject['data'].delete_if {|element| element['value'] == exception}
      lastValue = 0.0
      dataObject['data'].each { |element|
        if(element['value'] == exception) then 
          element['value'] = lastValue
        end
        lastValue = element['value']
      }
    end
    
    #p dataObject['data'].length
    #p dataObject['data'].index{|el| el[:value]==999.99}

    result = (dataObject['data'].inject(0.0) {|sum,element| (sum+element['value'])}/dataObject['data'].length()).round(2)
    return result
  end
end #}}}

def getWgtdAvgInTimeSlot(dataJson, start, stop, exceptions=[]) #{{{
  preDataObject=JSON.parse(dataJson)
  if(preDataObject['data'].nil? || preDataObject['data'].length == 0) then
    return nil
  end
  orderedData = preDataObject['data'].sort do |a,b|
    DateTime.strptime(a['timestamp'], '%Y-%m-%dT%H:%M:%S.%L') <=> DateTime.strptime(b['timestamp'], '%Y-%m-%dT%H:%M:%S.%L')
  end

  dataObject = getFullData(orderedData)

  if(dataObject['data'].nil? || dataObject['data'].length == 0) then
    return nil
  else
    timeSlotDataObject = getTimeSlotBasedOnBoundaries(dataObject, start, stop)
    exceptions.each do |exception|
      #timeSlotDataObject['data'].delete_if {|element| element['value'] == exception}
      lastValue = 0.0
      dataObject['data'].each { |element|
        if(element['value'] == exception) then 
          element['value'] = lastValue
        end
        lastValue = element['value']
      }
    end
    result = timeSlotDataObject['data'].length() != 0 ? (timeSlotDataObject['data'].inject(0.0) {|sum,element| sum+element['value']}/timeSlotDataObject['data'].length()).round(2) : nil

    #result = (timeSlotDataObject['data'].inject(0.0) {|sum,element| element['value'] != 999.99 ? sum+element['value'] : sum}/timeSlotDataObject['data'].length()).round(2)
    return result
  end
end #}}}

def getWgtdAvgInTimeSlotGap(dataJson, valueSpan, exceptions=[]) #{{{
  preDataObject=JSON.parse(dataJson)
  if(preDataObject['data'].nil? || preDataObject['data'].length == 0) then
    return nil
  end
  orderedData = preDataObject['data'].sort do |a,b|
    DateTime.strptime(a['timestamp'], '%Y-%m-%dT%H:%M:%S.%L') <=> DateTime.strptime(b['timestamp'], '%Y-%m-%dT%H:%M:%S.%L')
  end

  dataObject = getFullData(orderedData)

  if(dataObject['data'].nil? || dataObject['data'].length == 0) then
    return nil
  else
    timeSlotDataObject = getTimeSlotBasedOnGaps(dataObject, valueSpan)
    exceptions.each do |exception|
      #timeSlotDataObject['data'].delete_if {|element| element['value'] == exception}
      lastValue = 0.0
      dataObject['data'].each { |element|
        if(element['value'] == exception) then 
          element['value'] = lastValue
        end
        lastValue = element['value']
      }
    end

    if(timeSlotDataObject['data'].nil? || timeSlotDataObject['data'].length == 0) then
      return nil
    else
      result = timeSlotDataObject['data'].length() != 0 ? (timeSlotDataObject['data'].inject(0.0) {|sum,element| sum+element['value']}/timeSlotDataObject['data'].length()).round(2) : nil
      #result = (timeSlotDataObject['data'].inject(0.0) {|sum,element| element['value'] != 999.99 ? sum+element['value'] : sum}/timeSlotDataObject['data'].length()).round(2)
      return result
    end
  end
end #}}}

def getDtw(dataJson, exceptions=[]) #{{{
  preDataObject=JSON.parse(dataJson)
  if(preDataObject['data'].nil? || preDataObject['data'].length == 0) then
    return nil
  end
  orderedData = preDataObject['data'].sort do |a,b|
    DateTime.strptime(a['timestamp'], '%Y-%m-%dT%H:%M:%S.%L') <=> DateTime.strptime(b['timestamp'], '%Y-%m-%dT%H:%M:%S.%L')
  end

  exceptions.each do |exception|
    #orderedData.delete_if {|element| element['value'] == exception}
    lastValue = 0.0
    orderedData.each { |element|
      if(element['value'] == exception) then 
        element['value'] = lastValue
      end
      lastValue = element['value']
    }
  end
  #orderedFullData = getFullData(orderedData,10)
  orderedFullData = getFullData(orderedData,50)
=begin
  orderedValues = orderedData.map do |item|
    item['value']
  end
  p orderedValues
=end
  orderedFullValues = orderedFullData['data'].map do |item|
    item['value']
  end

  result = []
  Dir.new(File.join(__dir__,'training')).each_child() do |item|
    begin
      matchObject = item.match(/(.+)_values.json$/)
      uuid = matchObject.nil? ? nil : matchObject[1]
      traceData = JSON.load(File.open(File.join(__dir__,'training',"#{uuid}_values.json"), 'r'))
      traceData.keys
      orderedTraceData = traceData['Keyence Measurement'].sort do |a,b|
        DateTime.strptime(a['timestamp'], '%Y-%m-%dT%H:%M:%S.%L') <=> DateTime.strptime(b['timestamp'], '%Y-%m-%dT%H:%M:%S.%L')
      end
      exceptions.each do |exception|
        #orderedTraceData.delete_if {|element| element['value'] == exception}
        lastValue = 0.0
        orderedTraceData.each { |element|
          if(element['value'] == exception) then 
            element['value'] = lastValue
          end
          lastValue = element['value']
        }
      end
      #orderedFullTraceData = getFullData(orderedTraceData,10)
      orderedFullTraceData = getFullData(orderedTraceData,50)
=begin
      orderedTraceValues = orderedTraceData.map do |item|
        item['value']
      end
=end
      orderedFullTraceValues = orderedFullTraceData['data'].map do |item|
        item['value']
      end
      #dtw = Dtw::Naive.new(orderedValues, orderedTraceValues)  
      p orderedFullValues.length
      p orderedFullTraceValues.length
      dtw = Dtw::Naive.new(orderedFullValues, orderedFullTraceValues)  
      path = dtw.path
      sum = 0
      path.each do |pathElement|
        #sum = sum + (orderedValues[pathElement[0]]-orderedTraceValues[pathElement[1]]).abs
        sum = sum + (orderedFullValues[pathElement[0]]-orderedFullTraceValues[pathElement[1]]).abs
      end
      result.push({:uuid => uuid, :distance => sum, :traceLength => orderedFullTraceValues.length, :status => traceData['Zylinder Ø4,5-B Durchmesser Status'].last['value'], :scale => traceData['Zylinder Ø4,5-B Durchmesser Scale'].last['value']})
      #break
    end
  rescue Exception => e
    p e
    result.push({:uuid => uuid, :distance => nil})
  end
   

  if(orderedData.length == 0) then
    return nil
  else
    p result
    result = result.sort do |a,b|
      if(a[:distance].nil?) then
        1
      elsif(b[:distance].nil?) then
        -1
      else
        a[:distance] <=> b[:distance]
      end
    end
    return result
  end
end #}}}

def getDtwInTimeSlot(dataJson, start, stop, exceptions=[]) #{{{
  preDataObject=JSON.parse(dataJson)
  if(preDataObject['data'].nil? || preDataObject['data'].length == 0) then
    return nil
  end
  orderedData = preDataObject['data'].sort do |a,b|
    DateTime.strptime(a['timestamp'], '%Y-%m-%dT%H:%M:%S.%L') <=> DateTime.strptime(b['timestamp'], '%Y-%m-%dT%H:%M:%S.%L')
  end

  exceptions.each do |exception|
    #orderedData.delete_if {|element| element['value'] == exception}
    lastValue = 0.0
    orderedData.each { |element|
      if(element['value'] == exception) then 
        element['value'] = lastValue
      end
      lastValue = element['value']
    }
  end
  orderedFullData = getFullData(orderedData,10)

  orderedTimeSlotData = getTimeSlotBasedOnBoundaries(orderedFullData, start, stop)
  orderedTimeSlotValues = orderedTimeSlotData['data'].map do |item|
    item['value']
  end

  result = []
  Dir.new(File.join(__dir__,'training')).each_child() do |item|
    begin
      matchObject = item.match(/(.+)_values.json$/)
      uuid = matchObject.nil? ? nil : matchObject[1]
      traceData = JSON.load(File.open(File.join(__dir__,'training',"#{uuid}_values.json"), 'r'))
      traceData.keys
      orderedTraceData = traceData['Keyence Measurement'].sort do |a,b|
        DateTime.strptime(a['timestamp'], '%Y-%m-%dT%H:%M:%S.%L') <=> DateTime.strptime(b['timestamp'], '%Y-%m-%dT%H:%M:%S.%L')
      end
      exceptions.each do |exception|
        #orderedTraceData.delete_if {|element| element['value'] == exception}
        lastValue = 0.0
        orderedTraceData.each { |element|
          if(element['value'] == exception) then 
            element['value'] = lastValue
          end
          lastValue = element['value']
        }
      end
      orderedFullTraceData = getFullData(orderedTraceData,10)

      orderedTimeSlotTraceData = getTimeSlotBasedOnBoundaries(orderedFullTraceData, start, stop)
      orderedTimeSlotTraceValues = orderedTimeSlotTraceData['data'].map do |item|
        item['value']
      end

      #dtw = Dtw::Naive.new(orderedValues, orderedTraceValues)  
      p orderedTimeSlotValues.length
      p orderedTimeSlotTraceValues.length
      dtw = Dtw::Naive.new(orderedTimeSlotValues, orderedTimeSlotTraceValues)  
      path = dtw.path
      sum = 0
      p path
      path.each do |pathElement|
        #sum = sum + (orderedValues[pathElement[0]]-orderedTraceValues[pathElement[1]]).abs
        #sum = sum + (orderedTimeSlotValues[pathElement[0]]-orderedTimeSlotTraceValues[pathElement[1]]).abs
        sum = nil
      end
      result.push({:uuid => uuid, :distance => sum, :traceLength => orderedTimeSlotTraceValues.length, :status => traceData['Zylinder Ø4,5-B Durchmesser Status'].last['value'], :scale => traceData['Zylinder Ø4,5-B Durchmesser Scale'].last['value']})
      #break
    end
  rescue Exception => e
    p e
    result.push({:uuid => uuid, :distance => nil})
  end
   

  if(orderedData.length == 0) then
    return nil
  else
    p result
    result = result.sort do |a,b|
      if(a[:distance].nil?) then
        1
      elsif(b[:distance].nil?) then
        -1
      else
        a[:distance] <=> b[:distance]
      end
    end
    return result
  end
end #}}}

def getDtwInTimeSlotGap(dataJson, valueSpan, exceptions=[]) #{{{
  preDataObject=JSON.parse(dataJson)
  if(preDataObject['data'].nil? || preDataObject['data'].length == 0) then
    return nil
  end
  orderedData = preDataObject['data'].sort do |a,b|
    DateTime.strptime(a['timestamp'], '%Y-%m-%dT%H:%M:%S.%L') <=> DateTime.strptime(b['timestamp'], '%Y-%m-%dT%H:%M:%S.%L')
  end

  orderedFullData = getFullData(orderedData,1)

  #orderedTimeSlotData = getTimeSlotBasedOnBoundaries(orderedFullData, start, stop)
  orderedTimeSlotData = getTimeSlotBasedOnGaps(orderedFullData, valueSpan)
  exceptions.each do |exception|
    #orderedTimeSlotData['data'].delete_if {|element| element['value'] == exception}
    lastValue = 0.0
    orderedTimeSlotData['data'].each { |element|
      if(element['value'] == exception) then 
        element['value'] = lastValue
      end
      lastValue = element['value']
    }
  end
  orderedTimeSlotData = compactData(orderedTimeSlotData,10)

  orderedTimeSlotValues = orderedTimeSlotData['data'].map do |item|
    item['value']
  end

  result = []
  Dir.new(File.join(__dir__,'training')).each_child() do |item|
    begin
      matchObject = item.match(/(.+)_values.json$/)
      uuid = matchObject.nil? ? nil : matchObject[1]
      traceData = JSON.load(File.open(File.join(__dir__,'training',"#{uuid}_values.json"), 'r'))
      traceData.keys
      orderedTraceData = traceData['Keyence Measurement'].sort do |a,b|
        DateTime.strptime(a['timestamp'], '%Y-%m-%dT%H:%M:%S.%L') <=> DateTime.strptime(b['timestamp'], '%Y-%m-%dT%H:%M:%S.%L')
      end
      orderedFullTraceData = getFullData(orderedTraceData,1)

      #orderedTimeSlotTraceData = getTimeSlotBasedOnBoundaries(orderedFullTraceData, start, stop)
      orderedTimeSlotTraceData = getTimeSlotBasedOnGaps(orderedFullTraceData, valueSpan)
      exceptions.each do |exception|
        #orderedTimeSlotTraceData['data'].delete_if {|element| element['value'] == exception}
        lastValue = 0.0
        orderedTimeSlotTraceData['data'].each { |element|
          if(element['value'] == exception) then 
            element['value'] = lastValue
          end
          lastValue = element['value']
        }
      end
      orderedTimeSlotTraceData = compactData(orderedTimeSlotTraceData,10)
      orderedTimeSlotTraceValues = orderedTimeSlotTraceData['data'].map do |item|
        item['value']
      end

      #dtw = Dtw::Naive.new(orderedValues, orderedTraceValues)  
      p orderedTimeSlotValues.length
      p orderedTimeSlotTraceValues.length
      dtw = Dtw::Naive.new(orderedTimeSlotValues, orderedTimeSlotTraceValues)  
      path = dtw.path
      sum = 0
      path.each do |pathElement|
        #sum = sum + (orderedValues[pathElement[0]]-orderedTraceValues[pathElement[1]]).abs
        sum = sum + (orderedTimeSlotValues[pathElement[0]]-orderedTimeSlotTraceValues[pathElement[1]]).abs
      end
      result.push({:uuid => uuid, :distance => sum, :traceLength => orderedTimeSlotTraceValues.length, :status => traceData['Zylinder Ø4,5-B Durchmesser Status'].last['value'], :scale => traceData['Zylinder Ø4,5-B Durchmesser Scale'].last['value']})
      #break
    end
  rescue Exception => e
    p e
    result.push({:uuid => uuid, :distance => nil})
  end
   

  if(orderedData.length == 0) then
    return nil
  else
    p result
    result = result.sort do |a,b|
      if(a[:distance].nil?) then
        1
      elsif(b[:distance].nil?) then
        -1
      else
        a[:distance] <=> b[:distance]
      end
    end
    return result
  end
end #}}}

class Bar < Riddl::Implementation #{{{
  def response
    Riddl::Parameter::Complex.new("return","text/plain","hello world")
  end
end #}}}

class AggregatePost < Riddl::Implementation #{{{
  def response
    #pp @r[0]
    #pp @p
    body = nil
    @p.each do |parameter|
      #p parameter.to_json
      if(parameter.type == :body && parameter.value.instance_of?(Riddl::Parameter::Tempfile)) then
        body = parameter.value.read
      end
    end
    #p body
    response = (!body.nil? && !body['data'].nil?) ? {:min => getMin(body,[999.99]), :max => getMax(body,[999.99]), :avg => getAvg(body,[999.99]), :wgtdAvg => getWgtdAvg(body,[999.99]),:wgtdAvgSect4_1 => getWgtdAvgInTimeSlot(body,5200,9600,[999.99]), :wgtdAvgSect4_2 => getWgtdAvgInTimeSlotGap(body, [4,16.5], [999.99]), :dpNumber => getDpNumber(body,[999.99])} : {}
    #response = (!body.nil? && !body['data'].nil?) ? {:min => getMin(body,[999.99]), :max => getMax(body,[999.99]), :avg => getAvg(body,[999.99]), :wgtdAvg => getWgtdAvg(body,[999.99]),:wgtdAvgSect4_1 => getWgtdAvgInTimeSlot(body,5200,9600,[999.99]), :wgtdAvgSect4_2 => getWgtdAvgInTimeSlotGap(body, [4,16.5], [999.99]), :dtw => getDtw(body,[999.99]),:dtwSect4_1 => getDtwInTimeSlot(body,5200,9600,[999.99]), :dtwSect4_2 => getDtwInTimeSlotGap(body, [4,16.5], [999.99]), :dpNumber => getDpNumber(body,[999.99])} : {}
    #response = (!body.nil? && !body['data'].nil?) ? {:aaLoad_Z_min => getMin(body), :aaLoad_Z_max => getMax(body), :aaLoad_Z_avg => getAvg(body), :aaLoad_Z_wgtdAvg => getWgtdAvg(body), :aaLoad_Z_dpNumber => getDpNumber(body)} : {}
    #response = (!body.nil? && !body['data'].nil?) ? {:aaVactB_Z_min => getMin(body), :aaVactB_Z_max => getMax(body), :aaVactB_Z_avg => getAvg(body), :aaVactB_Z_wgtdAvg => getWgtdAvg(body), :aaVactB_Z_dpNumber => getDpNumber(body)} : {}
    #response = (!body.nil? && !body['data'].nil?) ? {:actSpeed_min => getMin(body), :actSpeed_max => getMax(body), :actSpeed_avg => getAvg(body), :actSpeed_wgtdAvg => getWgtdAvg(body), :actSpeed_dpNumber => getDpNumber(body)} : {}
    #response = (!body.nil? && !body['data'].nil?) ? {:driveLoad_min => getMin(body), :driveLoad_max => getMax(body), :driveLoad_avg => getAvg(body), :driveLoad_wgtdAvg => getWgtdAvg(body), :driveLoad_dpNumber => getDpNumber(body)} : {}
    #response = (!body.nil? && !body['data'].nil?) ? {:aaVactB_Z_min => getMin(body), :aaVactB_Z_max => getMax(body), :aaVactB_Z_avg => getAvg(body), :aaVactB_Z_wgtdAvg => getWgtdAvg(body), :aaVactB_Z_dpNumber => getDpNumber(body)} : {}
    #response = (!body.nil? && !body['data'].nil?) ? {:wgtdAvg => getWgtdAvg(body,[999.99]), :dpNumber => getDpNumber(body,[999.99])} : {}
    p response
    Riddl::Parameter::Complex.new("return","application/json",response.to_json)
  end
end #}}}

class AggregateWebSocket < Riddl::WebSocketImplementation #{{{
  def onopen
    puts "Connection established"
    Thread.new do
      1.upto 10 do |i|
        send("answer #{i}")
        sleep 1
      end
      close
    end
  end

  def onmessage(data)
    printf("Received: %p\n", data)
    send data
    printf("Sent: %p\n", data)
  end

  def onclose
    puts "Connection closed"
  end

end #}}}

Riddl::Server.new(File.dirname(__FILE__) + '/aggregator.xml', :port => 9998) do
  cross_site_xhr true

  on resource do
    run Bar if get '*'
    run AggregatePost if post 'contentJson'
    run AggregateWebSocket if websocket
  end
end.loop!
