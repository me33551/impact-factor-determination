#!/usr/bin/env ruby
require 'riddl/server'
require 'redis'
require 'json'

class ViewTraces < Riddl::Implementation
  def response
   begin
      redis = @a[0]
      keys = redis.keys('*')
      answer = Riddl::Parameter::Complex.new("return","application/json",keys.to_json)
    rescue Redis::BaseError
      p 'redis cannot connect'
      @status = 500
      answer = Riddl::Parameter::Complex.new("return","text/plain","redis cannot connect")
    end
    return answer
  end
end

class ViewTrace < Riddl::Implementation
  def response
   begin
      redis = @a[0]
      items = redis.lrange(@r[0],0,-1)
      answer = Riddl::Parameter::Complex.new("return","application/json",items.to_json)
    rescue Redis::BaseError
      p 'redis cannot connect'
      @status = 500
      answer = Riddl::Parameter::Complex.new("return","text/plain","redis cannot connect")
    end
    return answer
  end
end

class ViewTraceInfo < Riddl::Implementation
  def response
   begin
      redis = @a[0]
      item = redis.lindex(@r[0],0)
      answer = Riddl::Parameter::Complex.new("return","application/json",JSON.parse(item).to_json)
    rescue Redis::BaseError
      p 'redis cannot connect'
      @status = 500
      answer = Riddl::Parameter::Complex.new("return","text/plain","redis cannot connect")
    end
    return answer
  end
end

def viewTraceWithSub(uuid, redis)
  items = redis.lrange(uuid,1,-1)
  allItems = items.map do |item|
    instantiation = item.match /\"cpee:lifecycle:transition\":\"task\/instantiation\"/
    matchData = item.match /\"CPEE-INSTANCE-UUID\":\"(.*?)\"/
    subItems = nil
    if !instantiation.nil? && !matchData.nil?
      subItems = viewTraceWithSub(matchData[1],redis)
    end
    [item,*subItems]
  end
  allItems.flatten!
  return allItems
end
class ViewTraceIncludingSubtraces < Riddl::Implementation
  def response
    begin
      redis = @a[0]
      items = viewTraceWithSub(@r[0],redis)
      answer = Riddl::Parameter::Complex.new("return","application/json",items.to_json)
    rescue Redis::BaseError
      p 'redis cannot connect'
      @status = 500
      answer = Riddl::Parameter::Complex.new("return","text/plain","redis cannot connect")
    end
    return answer
  end
end


class PushJson < Riddl::Implementation
  def response
#    p @p[0]
    # evtl statt immer @p[0] noch auf richtigen parameter mit entsprechendem value ueberpruefen
    received = @p[0].value.read
#    p received

    begin
      json = JSON.parse(received).to_json
#      p json
#      p JSON.parse(received)
      redis = @a[0]
      redis.rpush(@r[0], received)
      answer = Riddl::Parameter::Complex.new("return","text/plain","hello world #{@r[0]} - #{@p}")
    rescue JSON::ParserError
      p 'json cannot be parsed'
      @status = 500
      answer = Riddl::Parameter::Complex.new("return","text/plain","json cannot be parsed")
    rescue Redis::BaseError
      p 'redis cannot connect'
      @status = 500
      answer = Riddl::Parameter::Complex.new("return","text/plain","redis cannot connect")
    end
    return answer
  end
end

class DummyStreamTrace < Riddl::SSEImplementation
  def onopen
    #send('opening')
    p "opening"
    Thread.new do
      counter=0
      while true do
        send counter
        p "sending #{counter}"
        sleep 1
        counter+=1
      end
      send('closing')
      close
    end
  end
  def onclose
    #send('closing')
    p "closing"
  end
end

class StreamTrace < Riddl::SSEImplementation
  def onopen
    #send('opening')
    p "opening"
    begin
      redis = @a[0]
      items = redis.lrange(@r[0],0,-1)
      p "requested items"
      #answer = Riddl::Parameter::Complex.new("return","application/json",items.to_json)
      Thread.new do
        counter=0
        items.each do |item|
          send item
          p "sending #{counter}"
          #sleep 1
          #sleep 0.1
          sleep 0.01
          counter+=1
         end
         #send('closing')
         close
       end
    rescue Redis::BaseError
      p 'redis cannot connect'
      @status = 500
      #answer = Riddl::Parameter::Complex.new("return","text/plain","redis cannot connect")
    end
  end
  def onclose
    send('closing')
    p "closing"
  end
end

def streamTraceWithSub(uuid, redis, globalCounter)
  begin
    items = redis.lrange(uuid,1,-1)
    p "requested items"
    counter=0
    items.each do |item|
      send item
      #p "sending #{counter}"
      p "sending #{globalCounter}"
      #sleep 1
      #sleep 0.1
      sleep 0.01
      counter+=1
      globalCounter+=1
      instantiation = item.match /\"cpee:lifecycle:transition\":\"task\/instantiation\"/
      matchData = item.match /\"CPEE-INSTANCE-UUID\":\"(.*?)\"/
      if !instantiation.nil? && !matchData.nil?
        globalCounter = streamTraceWithSub(matchData[1],redis,globalCounter)
      end
      #send('closing')
    end
    return globalCounter
    rescue Redis::BaseError
      p 'redis cannot connect'
      @status = 500
    end
end

class StreamTraceIncludingSubtraces < Riddl::SSEImplementation
  def onopen
    #send('opening')
    p "opening"
    Thread.new do
      streamTraceWithSub(@r[0],@a[0],0)
      close
    end
  end
  def onclose
    send('closing')
    p "closing"
  end
end

class Favicon < Riddl::Implementation
  def response
    #@headers.push(Riddl::Header.new("Content-Security-Policy", "default-src 'self'"))
    Riddl::Parameter::Complex.new("return","image/x-icon",File.read(File.join(__dir__, 'favicon.jpg')))
  end
end

Riddl::Server.new(File.dirname(__FILE__) + '/provider.xml', :port => 9999) do
  cross_site_xhr true
  
  begin
    redis=Redis.new(
      :path => "/tmp/redis.sock",
      :encoding => 'utf-8'
    )
    redis.select(15)
  rescue Redis::BaseError
    p "cannot connect to redis :("
    return
  end

  on resource do
    run ViewTraces, redis if get
    run DummyStreamTrace, redis if sse
    on resource 'favicon.ico' do
      run Favicon if get
    end
    on resource '[a-zA-Z0-9-]+' do
      #run ViewTrace, redis if get
      run ViewTraceIncludingSubtraces, redis if get
      #run PushJson, redis if post 'content'
      #run StreamTrace, redis if sse
      run StreamTraceIncludingSubtraces, redis if sse
      on resource 'info' do
        run ViewTraceInfo, redis if get
      end
    end
  end
end.loop!
