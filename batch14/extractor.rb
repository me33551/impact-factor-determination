#!/usr/bin/env ruby
require 'riddl/server'
require 'json'
require 'jsonpath'

def extractData(dataJson)
#    pp("onmessage extraction")
    #pp(dataJson)
    dataObject=JSON.parse(dataJson)
    sensors=dataObject['sensors'].nil? ? [] : dataObject['sensors']
    data=dataObject['data'].nil? ? [] : dataObject['data']
    extractedData=[];
    #pp sensors
    #pp data

    sensors.each do |sensorArrayElement|
      #pp "#{sensorArrayElement['sensor']['name']} - #{JSON.parse(sensorArrayElement['sensor']['extractor_arg'])['nav']}"
      values = JSON.parse(sensorArrayElement['sensor']['extractor_arg'])['nav'].nil? ? [] : JsonPath.on(data, JSON.parse(sensorArrayElement['sensor']['extractor_arg'])['nav'])
      timestamps = JSON.parse(sensorArrayElement['sensor']['extractor_arg'])['timestamp'].nil? ? [] : JsonPath.on(data, JSON.parse(sensorArrayElement['sensor']['extractor_arg'])['timestamp'])
      exceptions = JSON.parse(sensorArrayElement['sensor']['extractor_arg'])['exceptions'].nil? ? [] : JSON.parse(sensorArrayElement['sensor']['extractor_arg'])['exceptions']
      #pp values
      #pp timestamps
      #pp exceptions

      exceptions.each do |exception|
        firstIndexOfException=values.index(exception)
        while !firstIndexOfException.nil? do
          #pp "deleting #{firstIndexOfException}"
          if values.length==timestamps.length then
            timestamps.slice!(firstIndexOfException)
          end
          values.slice!(firstIndexOfException)
          firstIndexOfException=values.index(exception)
        end
      end

      timestamps.map! do |timestamp|
        #slicing each timestamp to 23 chars in order to get 'transform it to the right format' - this is not!!! a clean solution and should be changed
        #timestamp[0,23]
        timestamp[0..22]
      end

      dps = []
      values.zip(timestamps) do |array|
        dps.push({:value => array[0], :timestamp => array[1]})
      end

      #extractedData.push({:name => sensorArrayElement['sensor']['name'], :values => values, :timestamps => timestamps})
      extractedData.push({:name => sensorArrayElement['sensor']['name'], :dps => dps})
    end

  return extractedData
  #return {}
end

class Bar < Riddl::Implementation
  def response
    Riddl::Parameter::Complex.new("return","text/plain","hello world")
  end
end

class ExtractPost < Riddl::Implementation
  def response
    body = nil
    @p.each do |parameter|
      p parameter.to_json
      if(parameter.type == :body && parameter.value.instance_of?(Riddl::Parameter::Tempfile)) then
        body = parameter.value.read
        #p body
      end
    end
    response = !body.nil? ? extractData(body) : {}
    Riddl::Parameter::Complex.new("return","application/json",response.to_json)
  end
end

class ExtractWebSocket < Riddl::WebSocketImplementation
  def onopen
    puts "Connection established"
    Thread.new do
      1.upto 10 do |i|
        send("answer #{i}")
        sleep 1
      end
      close
    end
  end

  def onmessage(data)
    printf("Received: %p\n", data)
    send data
    printf("Sent: %p\n", data)
  end

  def onclose
    puts "Connection closed"
  end

end

Riddl::Server.new(File.dirname(__FILE__) + '/extractor.xml', :port => 9997) do
  cross_site_xhr true

  on resource do
    run Bar if get '*'
    run ExtractPost if post 'contentJson'
    run ExtractWebSocket if websocket
  end
end.loop!
