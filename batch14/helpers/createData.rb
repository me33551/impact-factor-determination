#!/usr/bin/env ruby
require 'logger'
require 'json'
require 'typhoeus'

logger = Logger.new File.new('createDataForDtw.log', 'w')

if(__FILE__ == $0) then
  p "hello world"
  if(ARGV.length == 1) then
    p "getting #{ARGV[0]}"
    request = Typhoeus::Request.new(
      "http://localhost:9999/#{ARGV[0]}/",
      method: :get
    )
    request.on_complete do |response|
      begin
        eventList = JSON.parse(response.response_body)
      rescue JSON::JSONError
        p "cannot parse response body (event list)"
        eventList = nil
      end
      dps = {}
      eventList.each do |eventJson|
        begin
          eventObject = JSON.parse(eventJson)
        rescue JSON::JSONError
          p "cannot parse event"
          eventObject = nil
        end
        data = eventObject.dig('event','data','data_receiver', 0, 'data')
        if(!data.nil? && (data.kind_of?(Array) || data.kind_of?(Hash))) then
          #p data
          extractRequest = Typhoeus::Request.new(
            "http://localhost:9997/",
            method: :post,
            headers: {"Content-Type" => "application/json;charset=UTF-8"},
            #body: {:sensors => [{"sensor": {"name":"Keyence Measurement","extractor_arg": "{\"nav\":\"$.[?(@.name=='measurement')].value\",\"timestamp\":\"$.[?(@.name=='measurement')].timestamp\",\"exceptions\":[999.99]}","extractor_url":"http://localhost:9997", "visualizer_url":"iframeScatterPlot.html"}}], :data => data}.to_json
            #body: {:sensors => [{"sensor": {"name":"gesamt","extractor_arg": "{\"nav\":\"$.raw.['OCR 3'].['Text']\"}","extractor_url":"http://localhost:9997", "visualizer_url":"iframeInfo.html"}}], :data => data}.to_json
            #body: {:sensors => [{"sensor": {"name":"Manual ⌀ 4.50 (+0.028, +0.040)","extractor_arg": "{\"nav\":\"$.result.[0].['⌀ 4.50 (+0.028, +0.040)']\"}","extractor_url":"http://localhost:9997", "visualizer_url":"iframeScatterPlot.html"}}], :data => data}.to_json
            #body: {:sensors => [{"sensor": {"name":"Manual ⌀ 4.50 (+0.028, +0.040)","extractor_arg": "{\"nav\":\"$.result.[0].['⌀ 4.50 (+0.028, +0.040)']\"}","extractor_url":"http://localhost:9997", "visualizer_url":"iframeScatterPlot.html"}},{"sensor": {"name":"Keyence Measurement","extractor_arg": "{\"nav\":\"$.[?(@.name=='measurement')].value\",\"timestamp\":\"$.[?(@.name=='measurement')].timestamp\",\"exceptions\":[]}","extractor_url":"http://localhost:9997", "visualizer_url":"iframeScatterPlot.html"}},{"sensor": {"name":"aaLoad_X","extractor_arg": "{\"nav\":\"$.[?(@.name=='Axis/X/aaLoad')].value\", \"timestamp\":\"$.[?(@.name=='Axis/X/aaLoad')].timestamp\"}","extractor_url":"http://localhost:9997/", "visualizer_url":"iframeScatterPlot.html"}},{"sensor": {"name":"aaLoad_Y","extractor_arg": "{\"nav\":\"$.[?(@.name=='Axis/Y/aaLoad')].value\", \"timestamp\":\"$.[?(@.name=='Axis/Y/aaLoad')].timestamp\"}","extractor_url":"http://localhost:9997/", "visualizer_url":"iframeScatterPlot.html"}},{"sensor": {"name":"Zylinder Ø4,5-B Durchmesser Status","extractor_arg": "{\"nav\":\"$.results.['Zylinder Ø4,5-B'].Durchmesser.status\"}","extractor_url":"http://localhost:9997", "visualizer_url":"iframeInfo.html"}},{"sensor": {"name":"Zylinder Ø4,5-B Durchmesser Scale","extractor_arg": "{\"nav\":\"$.results.['Zylinder Ø4,5-B'].Durchmesser.on_scale_from_zero_to_one\"}","extractor_url":"http://localhost:9997", "visualizer_url":"iframeInfo.html"}}], :data => data}.to_json
            body: {:sensors => [{"sensor": {"name":"Manual ⌀ 4.50 (+0.028, +0.040)","extractor_arg": "{\"nav\":\"$.result.[0].['⌀ 4.50 (+0.028, +0.040)']\"}","extractor_url":"http://localhost:9997", "visualizer_url":"iframeScatterPlot.html"}},{"sensor": {"name":"Keyence Measurement","extractor_arg": "{\"nav\":\"$.[?(@.name=='measurement')].value\",\"timestamp\":\"$.[?(@.name=='measurement')].timestamp\",\"exceptions\":[]}","extractor_url":"http://localhost:9997", "visualizer_url":"iframeScatterPlot.html"}},{"sensor": {"name":"aaLoad_X","extractor_arg": "{\"nav\":\"$.[?(@.name=='Axis/X/aaLoad')].value\", \"timestamp\":\"$.[?(@.name=='Axis/X/aaLoad')].timestamp\"}","extractor_url":"http://localhost:9997/", "visualizer_url":"iframeScatterPlot.html"}},{"sensor": {"name":"aaLoad_Y","extractor_arg": "{\"nav\":\"$.[?(@.name=='Axis/Y/aaLoad')].value\", \"timestamp\":\"$.[?(@.name=='Axis/Y/aaLoad')].timestamp\"}","extractor_url":"http://localhost:9997/", "visualizer_url":"iframeScatterPlot.html"}},{"sensor": {"name":"aaLoad_Z","extractor_arg": "{\"nav\":\"$.[?(@.name=='Axis/Z/aaLoad')].value\", \"timestamp\":\"$.[?(@.name=='Axis/Z/aaLoad')].timestamp\"}","extractor_url":"http://localhost:9997/", "visualizer_url":"iframeScatterPlot.html"}},{"sensor": {"name":"aaLeadP_X","extractor_arg": "{\"nav\":\"$.[?(@.name=='Axis/X/aaLeadP')].value\", \"timestamp\":\"$.[?(@.name=='Axis/X/aaLeadP')].timestamp\"}","extractor_url":"http://localhost:9997/", "visualizer_url":"iframeScatterPlot.html"}},{"sensor": {"name":"aaLeadP_Y","extractor_arg": "{\"nav\":\"$.[?(@.name=='Axis/Y/aaLeadP')].value\", \"timestamp\":\"$.[?(@.name=='Axis/Y/aaLeadP')].timestamp\"}","extractor_url":"http://localhost:9997/", "visualizer_url":"iframeScatterPlot.html"}},{"sensor": {"name":"aaLeadP_Z","extractor_arg": "{\"nav\":\"$.[?(@.name=='Axis/Z/aaLeadP')].value\", \"timestamp\":\"$.[?(@.name=='Axis/Z/aaLeadP')].timestamp\"}","extractor_url":"http://localhost:9997/", "visualizer_url":"iframeScatterPlot.html"}},{"sensor": {"name":"aaVactB_X","extractor_arg": "{\"nav\":\"$.[?(@.name=='Axis/X/aaVactB')].value\", \"timestamp\":\"$.[?(@.name=='Axis/X/aaVactB')].timestamp\"}","extractor_url":"http://localhost:9997/", "visualizer_url":"iframeScatterPlot.html"}},{"sensor": {"name":"aaVactB_Y","extractor_arg": "{\"nav\":\"$.[?(@.name=='Axis/Y/aaVactB')].value\", \"timestamp\":\"$.[?(@.name=='Axis/Y/aaVactB')].timestamp\"}","extractor_url":"http://localhost:9997/", "visualizer_url":"iframeScatterPlot.html"}},{"sensor": {"name":"aaVactB_Z","extractor_arg": "{\"nav\":\"$.[?(@.name=='Axis/Z/aaVactB')].value\", \"timestamp\":\"$.[?(@.name=='Axis/Z/aaVactB')].timestamp\"}","extractor_url":"http://localhost:9997/", "visualizer_url":"iframeScatterPlot.html"}},{"sensor": {"name":"driveLoad","extractor_arg": "{\"nav\":\"$.[?(@.name=='Spindle/driveLoad')].value\", \"timestamp\":\"$.[?(@.name=='Spindle/driveLoad')].timestamp\"}","extractor_url":"http://localhost:9997/", "visualizer_url":"iframeScatterPlot.html"}},{"sensor": {"name":"actSpeed","extractor_arg": "{\"nav\":\"$.[?(@.name=='Spindle/actSpeed')].value\", \"timestamp\":\"$.[?(@.name=='Spindle/actSpeed')].timestamp\"}","extractor_url":"http://localhost:9997/", "visualizer_url":"iframeScatterPlot.html"}},{"sensor": {"name":"Zylinder Ø4,5-B Durchmesser Status","extractor_arg": "{\"nav\":\"$.results.['Zylinder Ø4,5-B'].Durchmesser.status\"}","extractor_url":"http://localhost:9997", "visualizer_url":"iframeInfo.html"}},{"sensor": {"name":"Zylinder Ø4,5-B Durchmesser Scale","extractor_arg": "{\"nav\":\"$.results.['Zylinder Ø4,5-B'].Durchmesser.on_scale_from_zero_to_one\"}","extractor_url":"http://localhost:9997", "visualizer_url":"iframeInfo.html"}}], :data => data}.to_json
          )
          extractRequest.on_complete do |response|
            #p response.response_body
            begin
              #p response.response_body
              extractions = JSON.parse(response.response_body)
            rescue JSON::JSONError
              p "cannot parse response body (data points)"
              extractions = nil
            end
            extractions.each do |extraction|
              if(!extraction.nil? && !(extraction.dig('dps')).nil? && !(extraction.dig('dps')).empty?) then
                p extraction.dig('name')
                p extraction.dig('dps')
                if(!dps.has_key?(extraction.dig('name'))) then
                  dps[extraction.dig('name')]=[*extraction.dig('dps')]
                else
                  dps[extraction.dig('name')].push(*extraction.dig('dps'))
                end
              end
            end
          end
          extractRequest.run

        end
      end
      p "data points: "
      p dps
      p dps.length
      dps.each do |key,value|
        p key
        p value.length
      end
      File.open(File.join(__dir__,'examinedTraces',"#{ARGV[0]}_values.json"), "w+") do |file|
        file.write(dps.to_json)
      end
    end
    request.run
  else
    p "wrong number of arguments"
    p ARGV
  end
end
