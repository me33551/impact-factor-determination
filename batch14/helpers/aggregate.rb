#!/usr/bin/env ruby
require 'logger'
require 'json'
require 'typhoeus'

logger = Logger.new File.new('aggregate.log', 'w')

if(__FILE__ == $0) then
  if(ARGV.length == 4) then
    p ARGV[0]
    data = JSON.load(File.open("examinedTraces/#{ARGV[0]}_values.json",'r'))
    #p data
    request = Typhoeus::Request.new(
      "http://localhost:9998/",
      method: :post,
      headers: {"Content-Type" => "application/json"},
      body: {:data => data[ARGV[2]]}.to_json
      #body: {:data => data['Keyence Measurement']}.to_json
      #body: {:data => data['aaLoad_X']}.to_json
      #body: {:data => data['aaLoad_Y']}.to_json
      #body: {:data => data['aaLoad_Z']}.to_json
      #body: {:data => data['aaVactB_X']}.to_json
      #body: {:data => data['aaVactB_Y']}.to_json
      #body: {:data => data['aaVactB_Z']}.to_json
      #body: {:data => data['actSpeed']}.to_json
      #body: {:data => data['driveLoad']}.to_json
      #body: {:data => data['Manual ⌀ 4.50 (+0.028, +0.040)'].map {|item| {:value => item['value'].to_f, :timestamp => item['timestamp']}}}.to_json
      #body: {:data => [{"value" => 22.5, "timestamp" => "2020-01-01T00:00:00.000"}]}.to_json
    )
    request.on_complete do |response|
      if(response.success?)
        resp = nil
        if(ARGV[2] == "Keyence Measurement") then
          resp = JSON.parse(response.body)
        else
          resp = {}
          b = JSON.parse(response.body)
          resp["#{ARGV[2]}_min"] = b['min']
          resp["#{ARGV[2]}_max"] = b['max']
          resp["#{ARGV[2]}_avg"] = b['avg']
          resp["#{ARGV[2]}_wgtdAvg"] = b['wgtdAvg']
          resp["#{ARGV[2]}_dpNumber"] = b['dpNumber']
        end
        resp['status'] = nil
        resp['scale'] = nil
        resp['status'] = data['Zylinder Ø4,5-B Durchmesser Status'].last['value'] unless data['Zylinder Ø4,5-B Durchmesser Status'].nil?
        resp['scale'] = data['Zylinder Ø4,5-B Durchmesser Scale'].last['value'] unless data['Zylinder Ø4,5-B Durchmesser Status'].nil?
        p resp
        if(ARGV[1]=="replace") then
          JSON.dump(resp, File.open(File.join(__dir__,"#{ARGV[3]}", "#{ARGV[0]}_result.json"),'w+'))
        elsif(ARGV[1]=="add")
          newResults = JSON.load(File.open(File.join(__dir__,"#{ARGV[3]}", "#{ARGV[0]}_result.json"),'r+'))
          resp.each_pair do |key,value|
            newResults[key] = value
          end
          JSON.dump(newResults, File.open(File.join(__dir__,"#{ARGV[3]}", "#{ARGV[0]}_result.json"),'w+'))
        else
          p "ARGV[1] has wrong value ('replace' or 'add')"
      end 
      else
        p "request failed"
      end
    end
    request.run
  else
    p "wrong number of arguments"
    p ARGV
  end
end
