#!/usr/bin/env ruby
require 'logger'
require 'json'
require 'typhoeus'
require 'systemu'

logger = Logger.new File.new('startCreation.log', 'w')

if(__FILE__ == $0) then
  request = Typhoeus::Request.new(
    "http://localhost:9999/",
    method: :get
  )
  request.on_complete do |response|
    traceList = nil
    begin
      traceList = JSON.parse(response.body)
    rescue JSON::JSONError
        p "cannot parse response body (trace list)"
        traceList = []
    end
    traceList.each do |trace|
      #p trace
      requestTrace = Typhoeus::Request.new(
        "http://localhost:9999/#{trace}/info",
        method: :get
      )
      requestTrace.on_complete do |response|
        info = nil
        begin
          info = JSON.parse(response.body)
        rescue JSON::JSONError
          p "cannot parse response body (info)"
          info = {}
        end
        logger.debug info.dig('log','trace','cpee:name')
        #if(!info.dig('log','trace','cpee:name').nil? && ["GV12 Turn Production","GV12 Keyence Measuring"].include?(info.dig('log','trace','cpee:name'))) then
        if(!info.dig('log','trace','cpee:name').nil? && ["Spawn GV12 Production", "Spawn GV12 Production "].include?(info.dig('log','trace','cpee:name'))) then
          if(!info.dig('log','trace','cpee:uuid').nil?) then
            p info.dig('log','trace','cpee:uuid')
            p systemu("#{File.join(__dir__,'createData.rb')} #{info.dig('log','trace','cpee:uuid')}")
          elsif(!info.dig('log','trace','cpee:instance').nil?) then
            p info.dig('log','trace','cpee:instance')
            p systemu("#{File.join(__dir__,'createData.rb')} #{info.dig('log','trace','cpee:instance')}")
          else
            p "no uuid :("
          end
        end
      end
      requestTrace.run
    end
  end
  request.run
end
