#!/usr/bin/env ruby
require 'logger'
require 'systemu'

logger = Logger.new File.new('startAggregate.log', 'w')

if(__FILE__ == $0) then
  if(ARGV.length==0) then
    traces = Dir.new(File.join(__dir__,'examinedTraces'))
    counter = 0
    traces.each_child do |trace|
      matchObject = trace.match(/(.+)_values.json$/)
      uuid = matchObject.nil? ? nil : matchObject[1]
      #p systemu("#{File.join(__dir__,'aggregate.rb')} #{uuid} #{ARGV[0]}") unless uuid.nil?
      p systemu("#{File.join(__dir__,'aggregate.rb')} #{uuid} replace 'Keyence Measurement' 'examinedResults_keyence'") unless uuid.nil?
      p systemu("#{File.join(__dir__,'aggregate.rb')} #{uuid} replace 'aaLoad_X' 'examinedResults_machining'") unless uuid.nil?
      p systemu("#{File.join(__dir__,'aggregate.rb')} #{uuid} add 'aaLoad_Y' 'examinedResults_machining'") unless uuid.nil?
      p systemu("#{File.join(__dir__,'aggregate.rb')} #{uuid} add 'aaLoad_Z' 'examinedResults_machining'") unless uuid.nil?
      p systemu("#{File.join(__dir__,'aggregate.rb')} #{uuid} add 'aaVactB_X' 'examinedResults_machining'") unless uuid.nil?
      p systemu("#{File.join(__dir__,'aggregate.rb')} #{uuid} add 'aaVactB_Y' 'examinedResults_machining'") unless uuid.nil?
      p systemu("#{File.join(__dir__,'aggregate.rb')} #{uuid} add 'aaVactB_Z' 'examinedResults_machining'") unless uuid.nil?
      p systemu("#{File.join(__dir__,'aggregate.rb')} #{uuid} add 'actSpeed' 'examinedResults_machining'") unless uuid.nil?
      p systemu("#{File.join(__dir__,'aggregate.rb')} #{uuid} add 'driveLoad' 'examinedResults_machining'") unless uuid.nil?
      
=begin
      if(counter < 4) then
        p systemu("#{File.join(__dir__,'aggregate.rb')} #{uuid}") unless uuid.nil?
      end
=end
=begin
      if (counter>=0) then
        break;
      end
=end
      counter+=1
    end
  else
    p "wrong number of arguments"
    p ARGV
  end
end
