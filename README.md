TODOs for csv creation (it is recommended to just use the ones provided in the folder "csvs"):

preliminaries: install redis and ruby along with ruby gems (riddl,json,redis,logger,typhoeus,csv,psych,systemu,jsonpath,(dtw))
perform the following steps for batch14 and batch15 (files are contained in the corresponding folder and the subfolder "helpers"):
  - download log files of traces (http://cpee.org/~demo/DaSH/batch14.zip and http://cpee.org/~demo/DaSH/batch14.zip)
  - start redis db
  - change symbolic link "gv12" for both batches so that it points to the downloaded log files
  - save traces in redis db with "replayLogLocal.rb" (probably the path to the root files of the process need to be changed in the program)
  - start provider and extractor with "./provider.rb start -v" and "./provider.rb start -v"
  - run startCreation file -> writes traces in the folder "examinedTraces" ("./startCreation")
  - start aggregator "./aggregator.rb start -v" (provider and extractor can be stopped)
  - startAggregate starten ("./startAggregate.rb") -> writes results of aggregation into folders "examinedResults_keyence" and "examinedResults_machining"
  - aggregator can be stopped
  - start csvify to generate csv files ("./csvify.rb results14_keyence.csv examinedResults_keyence" and "./csvify.rb results14.csv examinedResults_machining") or ("./csvify.rb results15_keyence.csv examinedResults_keyence" and "./csvify.rb results15.csv examinedResults_machining") -> csv files are generated in the folder "batch14/helpers/csvs" or "batch15/helpers/csvs"


Afterwards, the provided R code can be executed after setting the paths for the csv input files and picture output files (at the start of the R files) - the following libraries are needed: pROC. "clean_code" creates the pictures used in the paper and "threshold_calculation" can be used to determine thresholds with the highest accuracy.
